
<?php

error_reporting(E_ERROR);
$con = mysqli_connect("localhost","root","1234","directory");
function dirToArray($dir) {
   $result = array(); 

   $cdir = scandir($dir); 
   foreach ($cdir as $key => $value) 
   { 
      if (!in_array($value,array(".",".."))) 
      { 
         if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
         { 
            $result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value); 
         } 
         else 
         { 
            $result[] = $value; 
         } 
      } 
   } 

   $query = 'insert into directory_Data ("directory") VALUE ("'.json_encode($result).'")';

   mysqli_query($query);

   return $result; 
} 

                
function getStructure($directory_datas){

    echo '<ul>';
    foreach($directory_datas as $key => $value){

        if(is_array($directory_datas[$key])){

            echo '<li data-jstree=\'{"opened":true,"selected":true}\'>'.$key;
            getStructure($value);
        }else{

            echo '<li>'.$value.'</li>';
        }
    }
    echo '</ul>';
}

?> 


<html>

    <head>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
    </head>

    <body>
        <div>

            <form method="post" action="" name="dir_form" class="dir_form">
                <input type="text" name="dir_path" id="dir_path" placeholder="Folder Path">
                <input type="submit" value="send">
            </form>
        </div>
         <div id="jstree_demo_div">
       
            <?php 
                if(!empty($_POST)){
                    $directory_data = dirToArray($_POST['dir_path']);
                    getStructure($directory_data);
                }
            ?>
         </div>


        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>

        <script type="text/javascript">
        $(function () {

            $('#jstree_demo_div').jstree();

            $('.dir_form').submit(function (e) {

                e.preventDefault();

                if($('#dir_path').val().trim() == ''){

                    alert('Please add a path');
                    return false;
                }

                return true;
            })
        });
        </script>
    </body>
</html>